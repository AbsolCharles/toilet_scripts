#! /bin/sh
#
# safeshell.sh
#
# Distributed under terms of the MIT license.
#

IFS='
'
#       The preceding value should be <space><tab><newline>.
#       Set IFS to its default value

\unalias -a
#       Unset all possible aliases.
#       Note that unalias is escaped to prevent an alias
#       being used for unalias.

unset -f command
#       Ensure command is not a user function.

PATH="$(command -p getconf _CS_PATH):$PATH"
#       Put on a reliable PATH prefix.

# ...

